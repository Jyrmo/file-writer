<?php

namespace JyrmoTest\File;

use Jyrmo\File\Writer;

class WriterTest extends \PHPUnit_Framework_TestCase {
    /**
     * @var Writer
     */
    private $writer;

    /**
     * @var string
     */
    private $inaccessibleDirPath;

    private function makeFile() : string {
        $tempDirPath = sys_get_temp_dir();
        $filePath = tempnam($tempDirPath, 'JyrmoTest_');

        return $filePath;
    }

    private function makeFilePath() : string {
        $filePath = $this->makeFile();
        unlink($filePath);

        return $filePath;
    }

    private function setWriterFile() {
        $filePath = $this->makeFile();
        $this->writer->setFilePath($filePath);
    }

    private function setWriterFilePath() {
        $filePath = $this->makeFilePath();
        $this->writer->setFilePath($filePath);
    }

    private function getWriterFileContent() : string {
        $filePath = $this->writer->getFilePath();
        $content = file_get_contents($filePath);

        return $content;
    }

    public function setUp() {
        $this->writer = new Writer();
    }

    public function testWriteToEmptyFile() {
        $this->setWriterFile();
        $filePath = $this->writer->getFilePath();
        $this->assertFileExists($filePath);
        $this->writer->write('content');
        $fileContent = $this->getWriterFileContent();
        $this->assertEquals('content', $fileContent);
    }

    public function testWriteToNonEmptyFile() {
        $this->setWriterFile();
        $filePath = $this->writer->getFilePath();
        file_put_contents($filePath, 'old content');
        $this->writer->write('overwritten content');
        $fileContent = $this->getWriterFileContent();
        $this->assertEquals('overwritten content', $fileContent);
    }

    public function testWriteToNonExistentFile() {
        $this->setWriterFilePath();
        $filePath = $this->writer->getFilePath();
        $this->assertFileNotExists($filePath);
        $this->writer->write('content');
        $fileContent = $this->getWriterFileContent();
        $this->assertEquals('content', $fileContent);
    }

    public function testWriteToInaccessibleFile() {
        $this->setWriterFile();
        $filePath = $this->writer->getFilePath();
        chmod($filePath, 0);
        $this->setExpectedException('Jyrmo\File\Exception\InaccessibleFileException');
        $this->writer->write('content');
    }

    public function testWriteToNotFile() {
        $dirPath = $this->makeFilePath();
        mkdir($dirPath);
        $this->writer->setFilePath($dirPath);
        $this->setExpectedException('Jyrmo\File\Exception\InvalidFileException');
        $this->writer->write('content');
    }

    public function testWriteToInaccessibleDir() {
        $dirPath = $this->makeFilePath();
        $this->inaccessibleDirPath = $dirPath;
        mkdir($dirPath);
        $filePath = tempnam($dirPath, 'test_');
        $this->writer->setFilePath($filePath);
        chmod($dirPath, 0);
        $this->setExpectedException('Jyrmo\File\Exception\InaccessibleFileException');
        $this->writer->write('content');
    }

    public function testWriteToSymlink() {
        $filePath = $this->makeFile();
        $symlinkPath = $this->makeFilePath();
        symlink($filePath, $symlinkPath);
        $this->writer->setFilePath($symlinkPath);
        $this->writer->write('content');
        $this->assertStringEqualsFile($filePath, 'content');
        unlink($filePath);
    }

    public function testWriteWithEmptyPath() {
        $this->writer->setFilePath('');
        $this->setExpectedException('Jyrmo\File\Exception\InvalidFileException');
        $this->writer->write('content');
    }

    public function testWriteStringable() {
        $this->setWriterFile();
        $stringable = new Stringable();
        $stringable->setStr('stringable content');
        $this->writer->write($stringable);
        $fileContent = $this->getWriterFileContent();
        $this->assertEquals('stringable content', $fileContent);
    }

    public function testWriteNonStringable() {
        $this->setWriterFile();
        $obj = new \stdClass();
        $obj->attr = 'val';
        $this->writer->write($obj);
        $fileContent = $this->getWriterFileContent();
        $this->assertEquals('', $fileContent);
    }

    public function testAppendToEmptyFile() {
        $this->setWriterFile();
        $this->writer->append('appended content');
        $fileContent = $this->getWriterFileContent();
        $this->assertEquals('appended content', $fileContent);
    }

    public function testAppendToNonEmptyFile() {
        $this->setWriterFile();
        $filePath = $this->writer->getFilePath();
        file_put_contents($filePath, 'old content ');
        $this->writer->append('new content');
        $fileContent = $this->getWriterFileContent();
        $this->assertEquals('old content new content', $fileContent);
    }

    public function testAppendToNonExistentFile() {
        $this->setWriterFilePath();
        $this->writer->append('appended content');
        $fileContent = $this->getWriterFileContent();
        $this->assertEquals('appended content', $fileContent);
    }

    public function testAppendToInaccessibleFile() {
        $this->setWriterFile();
        $filePath = $this->writer->getFilePath();
        chmod($filePath, 0);
        $this->setExpectedException('Jyrmo\File\Exception\InaccessibleFileException');
        $this->writer->append('appended content');
    }

    public function tearDown() {
        if (file_exists($this->inaccessibleDirPath)) {
            chmod($this->inaccessibleDirPath, 0777);
        }

        $filePath = $this->writer->getFilePath();
        if (is_file($filePath)) {
            unlink($filePath);
        } elseif (is_dir($filePath)) {
            rmdir($filePath);
        }

        if (file_exists($this->inaccessibleDirPath)) {
            rmdir($this->inaccessibleDirPath);
        }
    }
}

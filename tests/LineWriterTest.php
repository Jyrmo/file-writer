<?php

namespace JyrmoTest\File;

use Jyrmo\File\Writer;
use Jyrmo\File\LineWriter;

class LineWriterTest extends \PHPUnit_Framework_TestCase {
    /**
     * @var LineWriter
     */
    private $lineWriter;

    private function makeFile() : string {
        $tempDirPath = sys_get_temp_dir();
        $filePath = tempnam($tempDirPath, 'JyrmoTest_');

        return $filePath;
    }

    private function makeFilePath() : string {
        $filePath = $this->makeFile();
        unlink($filePath);

        return $filePath;
    }

    public function setUp() {
        $fileWriter = new Writer();
        $this->lineWriter = new LineWriter($fileWriter);
    }

    public function testConstructWithLineSeparator() {
        $fileWriter = new Writer();
        $lineWriter = new lineWriter($fileWriter, 'sep');
        $separator = $lineWriter->getLineSeparator();
        $this->assertEquals('sep', $separator);
    }

    public function testWriteLineToNonExistentFile() {
        $filePath = $this->makeFilePath();
        $this->assertFileNotExists($filePath);
        $this->lineWriter->setFilePath($filePath);
        $this->lineWriter->writeLine('line');
        $this->assertFileExists($filePath);
        $this->assertStringEqualsFile($filePath, 'line' . PHP_EOL);
    }

    public function testWriteLineToEmptyFile() {
        $filePath = $this->makeFile();
        $this->assertFileExists($filePath);
        $this->lineWriter->setFilePath($filePath);
        $this->lineWriter->writeLine('line');
        $this->assertStringEqualsFile($filePath, 'line' . PHP_EOL);
    }

    public function testWriteLineToNonEmptyFile() {
        $filePath = $this->makeFile();
        file_put_contents($filePath, 'old line');
        $this->lineWriter->setFilePath($filePath);
        $this->lineWriter->writeLine('new line');
        $this->assertStringEqualsFile($filePath, 'new line' . PHP_EOL);
    }

    public function AppendLineToEmptyFile() {
        $filePath = $this->makeFile();
        $this->lineWriter->setFilePath($filePath);
        $this->lineWriter->appendLine('appended line');
        $this->assertStringEqualsFile($filePath, 'appended line' . PHP_EOL);
    }

    public function testAppendLineToNonEmptyFile() {
        $filePath = $this->makeFile();
        file_put_contents($filePath, 'old line');
        $this->lineWriter->setFilePath($filePath);
        $this->lineWriter->appendLine(' new line');
        $this->assertStringEqualsFile($filePath, 'old line new line' . PHP_EOL);
    }

    public function testChangeLineSeparator() {
        $filePath = $this->makeFile();
        $this->lineWriter->setFilePath($filePath);
        $defaultSep = $this->lineWriter->getLineSeparator();
        $this->assertEquals(PHP_EOL, $defaultSep);
        $this->lineWriter->setLineSeparator('---');
        $newSeparator = $this->lineWriter->getLineSeparator();
        $this->assertEquals('---', $newSeparator);
        $this->lineWriter->writeLine('first line');
        $this->assertStringEqualsFile($filePath, 'first line---');
        $this->lineWriter->appendLine('second line');
        $this->assertStringEqualsFile($filePath, 'first line---second line---');
    }

    public function testWriteLinesToEmptyFile() {
        $lines = array(
            'first line',
            'second line',
        );
        $filePath = $this->makeFile();
        $this->lineWriter->setFilePath($filePath);
        $this->lineWriter->writeLines($lines);
        $this->assertStringEqualsFile($filePath, 'first line' . PHP_EOL . 'second line' . PHP_EOL);
    }

    public function testWriteLinesSingleLine() {
        $lines = array('line');
        $filePath = $this->makeFile();
        $this->lineWriter->setFilePath($filePath);
        $this->lineWriter->writeLines($lines);
        $this->assertStringEqualsFile($filePath, 'line' . PHP_EOL);
    }

    public function testWriteLinesNoLines() {
        $lines = array();
        $filePath = $this->makeFile();
        $this->lineWriter->setFilePath($filePath);
        $this->lineWriter->writeLines($lines);
        $this->assertStringEqualsFile($filePath, '');
    }

    public function testWriteLinesToNonEmptyFile() {
        $lines = array(
            'first line',
            'second line'
        );
        $filePath = $this->makeFile();
        file_put_contents($filePath, 'old content');
        $this->assertStringEqualsFile($filePath, 'old content');
        $this->lineWriter->setFilePath($filePath);
        $this->lineWriter->writeLines($lines);
        $this->assertStringEqualsFile($filePath, 'first line' . PHP_EOL . 'second line' . PHP_EOL);
    }

    public function testWriteLinesChangeSeparator() {
        $lines = array(
            'first line',
            'second line'
        );
        $filePath = $this->makeFile();
        $this->lineWriter->setFilePath($filePath);
        $this->lineWriter->setLineSeparator('---');
        $this->lineWriter->writeLines($lines);
        $this->assertStringEqualsFile($filePath, 'first line---second line---');
    }

    public function testAppendLinesToEmptyFile() {
        $lines = array(
            'first appended line',
            'second appended line'
        );
        $filePath = $this->makeFile();
        $this->lineWriter->setFilePath($filePath);
        $this->lineWriter->appendLines($lines);
        $this->assertStringEqualsFile($filePath, 'first appended line' . PHP_EOL . 'second appended line' . PHP_EOL);
    }

    public function testAppendLinesToNonEmptyFile() {
        $lines = array(
            'first appended line',
            'second appended line'
        );
        $filePath = $this->makeFile();
        file_put_contents($filePath, 'old content');
        $this->lineWriter->setFilePath($filePath);
        $this->lineWriter->appendLines($lines);
        $this->assertStringEqualsFile($filePath, 'old contentfirst appended line' . PHP_EOL . 'second appended line' . PHP_EOL);
    }

    public function tearDown() {
        $filePath = $this->lineWriter->getFilePath();
        if (file_exists($filePath)) {
            unlink($filePath);
        }
    }
}

<?php

namespace JyrmoTest\File;

class Stringable {
    /**
     * @var string
     */
    protected $str;

    public function setStr(string $str) {
        $this->str = $str;
    }

    public function __toString() : string {
        return $this->str;
    }
}

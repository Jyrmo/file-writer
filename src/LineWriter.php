<?php

namespace Jyrmo\File;

class LineWriter {
    /**
     * @var Writer
     */
    protected $fileWriter;

    /**
     * @var string
     */
    protected $lineSeparator;

    protected function linesToString(array $lines) : string {
        $strLines = empty($lines) ? '' : join($this->lineSeparator, $lines) . $this->lineSeparator;

        return $strLines;
    }

    public function setFileWriter(Writer $fileWriter) {
        $this->fileWriter = $fileWriter;
    }

    public function getFileWriter() : Writer {
        return $this->fileWriter;
    }

    public function setLineSeparator(string $lineSeparator) {
        $this->lineSeparator = $lineSeparator;
    }

    public function getLineSeparator() : string {
        return $this->lineSeparator;
    }

    public function setFilePath(string $filePath) {
        $this->fileWriter->setFilePath($filePath);
    }

    public function getFilePath() : string {
        $filePath = $this->fileWriter->getFilePath();

        return $filePath;
    }

    public function __construct(Writer $fileWriter, string $lineSeparator = PHP_EOL) {
        $this->setFileWriter($fileWriter);
        $this->setLineSeparator($lineSeparator);
    }

    public function writeLine(string $line) {
        $this->fileWriter->write($line . $this->lineSeparator);
    }

    public function writeLines(array $lines) {
        $strLines = $this->linesToString($lines);
        $this->fileWriter->write($strLines);
    }

    public function appendLine(string $line) {
        $this->fileWriter->append($line . $this->lineSeparator);
    }

    public function appendLines(array $lines) {
        $strLines = $this->linesToString($lines);
        $this->fileWriter->append($strLines);
    }
}

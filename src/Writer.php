<?php

namespace Jyrmo\File;

use Jyrmo\File\Exception\InaccessibleFileException;
use Jyrmo\File\Exception\InvalidFileException;

class Writer {
    /**
     * @var string
     */
    protected $filePath;

    /**
    * @throws FileException
    */
    protected function validateFilePath() {
        if (file_exists($this->filePath)) {
            if (is_file($this->filePath)) {
                if (!is_writable($this->filePath)) {
                    throw new InaccessibleFileException('The file "' . $this->filePath . '" is inaccessible for writing.');
                }
            } else {
                throw new InvalidFileException('The path "' . $this->filePath . '" does not refer to a file.');
            }
        } else {
            $dirPath = dirname($this->filePath);
            if (file_exists($dirPath)) {
                if (is_dir($dirPath)) {
                    if (!is_writable($dirPath)) {
                        throw new InaccessibleFileException('The file "' . $this->filePath . '" is inaccessible for writing.');
                    }
                } else {
                    throw new InvalidFileException('The path "' . $this->filePath . '" does not refer to a file.');
                }
            } else {
                throw new InvalidFileException('The path "' . $this->filePath . '" does not refer to a file.');
            }
        }
    }

    public function setFilePath(string $filePath) {
        $this->filePath = $filePath;
    }

    public function getFilePath() : string {
        return $this->filePath;
    }

    public function __construct(string $filePath = '') {
        $this->setFilePath($filePath);
    }

    public function write($content) {
        $this->validateFilePath();
        file_put_contents($this->filePath, $content, LOCK_EX);
    }

    public function append($content) {
        $this->validateFilePath();
        file_put_contents($this->filePath, $content, FILE_APPEND | LOCK_EX);
    }
}

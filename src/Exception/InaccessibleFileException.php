<?php

namespace Jyrmo\File\Exception;

class InaccessibleFileException extends FileException {}

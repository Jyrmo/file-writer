<?php

namespace Jyrmo\File\Exception;

class InvalidFileException extends FileException {}
